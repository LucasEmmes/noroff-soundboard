import QtQuick
import QtQuick.Controls 2.15
import QtMultimedia

Window {
    id: root;
    width: 640
    height: 640
    visible: true
    title: qsTr("SoundBoard")

//    property string buttonColor: "#2A9D8F";
    property string headerColor: "#264653";
    property string backgroundColor: "#E9C46A";
    property string titleColor: "#E76F51";
    property string volumeColor: "#264653";
    property var soundEffects: [
        "qrc:/soundeffects/amogus.wav",
        "qrc:/soundeffects/android.wav",
        "qrc:/soundeffects/augh.wav",
        "qrc:/soundeffects/bonk.wav",
        "qrc:/soundeffects/bruh.wav",
        "qrc:/soundeffects/buzzer.wav",
        "qrc:/soundeffects/clashRoyale.wav",
        "qrc:/soundeffects/discord.wav",
        "qrc:/soundeffects/emotionalDamage.wav",
        "qrc:/soundeffects/fart.wav",
        "qrc:/soundeffects/fbi.wav",
        "qrc:/soundeffects/hogRider.wav",
        "qrc:/soundeffects/pufferfish.wav",
        "qrc:/soundeffects/tacoBell.wav",
        "qrc:/soundeffects/vine.wav",
        "qrc:/soundeffects/yeet.wav",
    ];

    function intToHex(integer) {
        var str = Number(integer).toString(16);
        return str.length == 1 ? "0" + str : str;
    }

    function getColor(number) {
        let x = number%4;
        let y = ~~(number/4);

        let r = 100;
        let g = 255*(y/4);
        let b = 255*(x/4);

        return intToHex(~~r) + intToHex(~~g) + intToHex(~~b);
    }

    function makeButtonColors() {
        let colors = [];
        for (let i = 0; i < 16; i++) {
            colors.push("#"+getColor(i));
        }
        return colors;
    }

    property var buttonColor: makeButtonColors();


    Rectangle {
        anchors.fill: parent;
        color: backgroundColor;
    }

    Column {
        anchors.horizontalCenter: parent.horizontalCenter;
        anchors.top: root.top;
        spacing: 30

        Rectangle {
            width: parent.parent.width;
            height: 60;
            color: headerColor;
            Text {
                text: qsTr("- SoundBoard -")
                anchors.centerIn: parent;
                color: titleColor;
                font.family: "Poor Richard";
                font.pointSize: 25;
            }
        }

        Row {
            spacing: 10;
            height: 40;
            anchors.horizontalCenter: parent.horizontalCenter;
            Label {
                text: "Last played: "
                font.pointSize: 20;
                color: volumeColor;
            }
            Label {
                id: lastPlayedLabel;
                text: ""
                font.pointSize: 20;
                color: volumeColor;
            }
        }

        Grid {
            columns: 4;
            spacing: 20;
            anchors.horizontalCenter: parent.horizontalCenter;

            Repeater {
                model: 16;
                Item {
                    height: root.height < root.width ? ~~(root.height/8) : ~~(root.width/8);
                    width: this.height;

                    SoundEffect {
                        id: soundEffect;
                        source: soundEffects[index];
                        volume: 0.5;
                    }

                    Rectangle {
                        id: soundEffectButton;
                        property int personalNumber: index
                        property string soundEffectName: "\""+soundEffects[index].slice(18,-4)+"\"";
                        anchors.fill: parent;
                        color: buttonColor[index];
                        radius: 5;
                        SequentialAnimation on color {
                            running: false;
                            id: animationClicked;
                            ColorAnimation {
                                to: Qt.lighter(root.buttonColor[soundEffectButton.personalNumber], 1.8);
                                duration: 250;
                            }
                            ColorAnimation {
                                to: Qt.lighter(root.buttonColor[soundEffectButton.personalNumber], 1.2);
                                duration: 250;
                            }
                        }
                        MouseArea {
                            anchors.fill: parent;
                            hoverEnabled: true;
                            onPressed: {
                                soundEffect.volume = volumeSlider.value / 100;
                                soundEffect.play();
                                lastPlayedLabel.text = parent.soundEffectName;
                            }
                            onEntered: {
                                parent.color = Qt.lighter(buttonColor[parent.personalNumber], 1.2);
                            }
                            onReleased: {
                                parent.color = buttonColor[parent.personalNumber];
                                animationClicked.restart();
                            }
                            onExited: {
                                parent.color = buttonColor[parent.personalNumber];
                            }
                        }
                    }
                }
            }
        }

        Row {
            anchors.horizontalCenter: parent.horizontalCenter;
            spacing: 20;

            Label {
                text: "Volume";
                font.pointSize: 20;
                color: volumeColor;
            }

            Slider {
                id: volumeSlider;
                anchors.verticalCenter: parent.verticalCenter;
                width: 200;
                from: 0;
                to: 100;
                value: 50;
            }
        }
    }
}
